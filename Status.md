# Dev Status / Notes

## Nov 14, 2023
Created the repo with:
- minimal cli
- ErrorHandler class
- README
- PrintMode enum
- Ability to log & print messages

PROBLEM: I designed it with OUT libraries in mind. Since it uses static instance, logging can't be customized per-library using it, which means log files could be written from multiple libraries! This could be a problem. I definitely need SOME kind of solution.

PROBLEM: Instantiating via config is super easy. But instantiating manually is awful & there are no smart defaults.

PROBLEM: There is NO consideration of log file length. Log files NEED to have a maximum length
