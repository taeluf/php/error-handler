<?php

namespace Tlf\ErrorHandler;

class Cli extends \Tlf\Cli {

    /** array<string command, string help_message> */
    protected array $commands = [
        'init' => "Create a config file and php initialization script", 

    ];

    public function init_commands(){
        $cli = $this;

        $cli->load_command('main',  
            function($cli, $args){  
                $cli->call_command('help',[]);
            }, 'Show this help menu'  
        );  

        foreach ($this->commands as $name => $help_message){
            $cli->load_command($name, [$this, 'run_'.$name], $help_message);
        }

    }

    public function run_init() {
        $dir = getcwd();
        if ($this->ask("Create 'config/tlferr.json' in '$dir'")){
            $input_config = dirname(__DIR__).'/config/tlferr.json';
            file_put_contents($dir.'/config/tlferr.json', file_get_contents($input_config));
            $this->notice("Created config/tlferr.json", "");
        }

        echo "\n\n";
        echo "# Copy+paste the following into a PHP file to initialize the logging library:\n\n";

        echo file_get_contents(__DIR__.'/sample-setup.php');

        echo "\n\n\n";

    }
}
