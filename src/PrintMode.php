<?php

namespace Tlf\ErrorHandler;

enum PrintMode : string {
    case PRINT_ALL = "print_all";
    case PRINT_DEBUG = "print_debug";
    case PRINT_USER_FRIENDLY = "print_user_friendly";
    case PRINT_NONE = "print_none";
}
