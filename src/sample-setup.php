<?php

$project_root_dir = __DIR__;
$error_handler = \Tlf\ErrorHandler::init($project_root_dir, 'config/tlferr.json');
$error_handler->print_mode = \Tlf\ErrorHandler::determine_print_mode(); # PRINT_DEBUG unless running in a non-localhost http environment
// Or use enum \Tlf\ErrorHandler\Mode:
# \Tlf\ErrorHandler\Mode::PRINT_ALL
# \Tlf\ErrorHandler\Mode::PRINT_DEBUG
# \Tlf\ErrorHandler\Mode::PRINT_USER_FRIENDLY
# \Tlf\ErrorHandler\Mode::PRINT_NONE
