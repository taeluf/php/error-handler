<?php

namespace Tlf;

class ErrorHandler {

    static protected ErrorHandler $instance;

    /**
     * absolute path to the root of your php application
     */
    protected string $root_dir;

    /**
     * Max number of characters from a log message to output to the log file
     * default is 100.
     */
    public int $max_log_msg_chars = 100;

    /**
     * print mode
     */
    public \Tlf\ErrorHandler\PrintMode $print_mode;

    /**
     * absolute path to log dir
     */
    public string $log_dir;

    /** 
     * Log the given message & args to the given file. May print, depending on your instance's print_mode
     * @param $user_message string to show a regular user of your software
     * @param $debug_message string [optional] to show developers of your software
     * @param $log_file string log file to write to. defaults to main.txt
     * @param $args_to_log array [optional] array of arguments to log. Prints MINIMAL object and array information
     */
    static public function log(string $user_message, string $debug_message='', string $log_file = 'main.txt', array $args_to_log = []){
        if (!isset(self::$instance)){
            throw new \Exception("\\Tlf\\ErrorHandler instance not setup. Call \\Tlf\\ErrorHandler\\init(...) before using log()");
        }

        self::$instance->log_message($user_message, $debug_message, $log_file, $args_to_log);
    }

    /**
     * Initialize an ErrorHandler & set the static instance to the new handler.
     *
     * @param string $app_root_dir string root of your php application
     * @param $relative_config_file string relative file path to a .json config file
     * @return \Tlf\ErrorHandler the new error handler
     */
    static public function init(string $app_root_dir, string $relative_config_file): \Tlf\ErrorHandler {
        $path = $app_root_dir.'/'.$relative_config_file;
        if (!is_file($path))throw new \Exception("File '$relative_config_file' does not exist in dir '$app_root_dir'.\n");
        $configs = json_decode(file_get_contents($path),true);
        if (!is_array($configs)){
            throw new \Exception("File '$relative_config_file' does contains invalid json in dir '$app_root_dir'.\n");
        }
        return self::init_from_config($app_root_dir, $configs);
    }
    /**
     * @see \Tlf\ErrorHandler::init()
     */
    static public function init_from_config(string $app_root_dir, array $configs): \Tlf\ErrorHandler {
        $new_handler = new self($app_root_dir);

        $new_handler->log_dir = $app_root_dir .'/'. $configs['log_dir'];
        if (!is_dir($new_handler->log_dir)){
            mkdir($new_handler->log_dir, 0754, true);
        }
        $new_handler->print_mode = \Tlf\ErrorHandler\PrintMode::from($configs['print_mode']);

        if (isset($configs['max_log_msg_chars'])){
            $new_handler->max_log_msg_chars = $configs['max_log_msg_chars'];
        }

        if ($configs['handle_exception'] === true){
            $new_handler->register_exception_handler();
        }
        if ($configs['handle_php_error_log'] === true){
            $new_handler->register_error_log_handler();
        }

        self::$instance = $new_handler;

        return $new_handler;
    }

    public function __construct(string $app_root_dir){
        $this->root_dir = $app_root_dir;
    }

    /**
     * @see \Tlf\ErrorHandler::log()
     */
    public function log_message(string $user_message, string $debug_message = '', string $log_file = 'main.txt', array $args_to_log = []){
        $datetime = date("c");

        $args_message = "Args logging not yet supported";

        $message = '';
        $message .= '['.$datetime.']: ';
        $message .= "\n  User: $user_message";
        $message .= "\n  Debug: $debug_message";
        $message .= "\n  Args: $args_message";

        switch ($this->print_mode){
            case \Tlf\ErrorHandler\PrintMode::PRINT_ALL:
                echo $message;
                break;
            case \Tlf\ErrorHandler\PrintMode::PRINT_DEBUG:
                echo "\n  $debug_message";
                break;
            case \Tlf\ErrorHandler\PrintMode::PRINT_USER_FRIENDLY:
                echo "$user_message";
                break;
            case \Tlf\ErrorHandler\PrintMode::PRINT_NONE:
                // do nothing
                break;
        }


        if (($count = strlen($user_message)) > $this->max_log_msg_chars){
            $user_message = substr($user_message, 0, $this->max_log_msg_chars)."[{$count} chars]";
        }
        if (($count = strlen($debug_message)) > $this->max_log_msg_chars){
            $debug_message = substr($debug_message, 0, $this->max_log_msg_chars)."[{$count} chars]";
        }
        if (($count = strlen($args_message)) > $this->max_log_msg_chars){
            $debug_message = substr($args_message, 0, $this->max_log_msg_chars)."[{$count} chars]";
        }

        $message = '';
        $message .= '['.$datetime.']: ';
        $message .= "\n  User: $user_message";
        $message .= "\n  Debug: $debug_message";
        $message .= "\n  Args: $args_message";

        $fh = fopen($this->log_dir.'/'.$log_file,'a');
        fwrite($fh, "\n".$message);
        fclose($fh);

    }

    public function determine_print_mode(): \Tlf\ErrorHandler\PrintMode {
        // determine environment
    }

    public function register_exception_handler(){
    }
    public function register_error_log_handler(){
    }
}
