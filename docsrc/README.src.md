# PHP Error Handler
A PHP error handler w/ simple configuration and high ease-of-use.

UNDER DEVELOPMENT:
- v1.0 may include breaking changes for the forseeable future.
- not all documented features are currently implemented

## Install
@template(php/composer_install)

## Usage
First setup per instructions below.

```php
<?php

$args_to_log = ['exception'=> new \Exception("Error here!")];
\Tlf\ErrorHandler::log("User Mesage only ...");
// or include a separate debug message, custom log file name, and array of args to log
\Tlf\ErrorHandler::log('User Message ...', 'Debug Message...', 'main.txt', $args_to_log);
```

You may, in your project, create a no namespaced `log()` function that calls the error handler, just to save keystrokes.

You may use an instance of ErrorHandler (via constructor or init) instead, but its more cumbersome that way.

## Setup
Run `vendor/bin/tlferr init` to create the config file for you & print a php setup script, for you to copy+paste.

`config/tlferr.json`:
```json
@file(config/tlferr.json)
```

PHP Initialization:
```php
@file(src/sample-setup.php)
```

`print_mode` can be set in config with lower-case version of the enums above, like `"print_all"` or `"print_none"`

