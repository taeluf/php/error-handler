<?php

namespace Tlf\ErrorHandler\Test;

class Main extends \Tlf\Tester {


    public function testMain(){
        $root_dir = $this->file('test');
        $handler = \Tlf\ErrorHandler::init_from_config(
            $root_dir,
            [
                'log_dir'=> "output/logs",
                'print_mode' => "print_all",
                'handle_exception' => true,
                'handle_php_error_log' => true,
            ]
        );

        \Tlf\ErrorHandler::log("Logging a message", "Line '".__LINE__."' in file '".__FILE__."'", 'main.txt', []);

    }

}

