<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# PHP Error Handler  
A PHP error handler w/ simple configuration and high ease-of-use.  
  
UNDER DEVELOPMENT:  
- v1.0 may include breaking changes for the forseeable future.  
- not all documented features are currently implemented  
  
## Install  
```bash  
composer require taeluf/error-handler v1.0.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/error-handler": "v1.0.x-dev"}}  
```  
  
  
## Usage  
First setup per instructions below.  
  
```php  
<?php  
  
$args_to_log = ['exception'=> new \Exception("Error here!")];  
\Tlf\ErrorHandler::log("User Mesage only ...");  
// or include a separate debug message, custom log file name, and array of args to log  
\Tlf\ErrorHandler::log('User Message ...', 'Debug Message...', 'main.txt', $args_to_log);  
```  
  
You may, in your project, create a no namespaced `log()` function that calls the error handler, just to save keystrokes.  
  
You may use an instance of ErrorHandler (via constructor or init) instead, but its more cumbersome that way.  
  
## Setup  
Run `vendor/bin/tlferr init` to create the config file for you & print a php setup script, for you to copy+paste.  
  
`config/tlferr.json`:  
```json  
{  
    "log_dir": "logs",  
    "print_mode": "print_all",  
    "handle_exceptions": true,  
    "handle_php_error_log": true,  
    "max_log_msg_chars": 100  
}  
```  
  
PHP Initialization:  
```php  
<?php  
  
$project_root_dir = __DIR__;  
$error_handler = \Tlf\ErrorHandler::init($project_root_dir, 'config/tlferr.json');  
$error_handler->print_mode = \Tlf\ErrorHandler::determine_print_mode(); # PRINT_DEBUG unless running in a non-localhost http environment  
// Or use enum \Tlf\ErrorHandler\Mode:  
# \Tlf\ErrorHandler\Mode::PRINT_ALL  
# \Tlf\ErrorHandler\Mode::PRINT_DEBUG  
# \Tlf\ErrorHandler\Mode::PRINT_USER_FRIENDLY  
# \Tlf\ErrorHandler\Mode::PRINT_NONE  
```  
  
`print_mode` can be set in config with lower-case version of the enums above, like `"print_all"` or `"print_none"`  
  
