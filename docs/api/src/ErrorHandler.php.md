<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ErrorHandler.php  
  
# class Tlf\ErrorHandler  
  
See source code at [/src/ErrorHandler.php](/src/ErrorHandler.php)  
  
## Constants  
  
## Properties  
- `static protected ErrorHandler $instance;`   
- `protected string $root_dir;` absolute path to the root of your php application  
- `public int $max_log_msg_chars = 100;` Max number of characters from a log message to output to the log file  
default is 100.  
- `public \Tlf\ErrorHandler\PrintMode $print_mode;` print mode  
- `public string $log_dir;` absolute path to log dir  
  
## Methods   
- `static public function log(string $user_message, string $debug_message='', string $log_file  'main.txt', array $args_to_log  [])` Log the given message & args to the given file. May print, depending on your instance's print_mode  
- `static public function init(string $app_root_dir, string $relative_config_file): \Tlf\ErrorHandler` Initialize an ErrorHandler & set the static instance to the new handler.  
  
- `static public function init_from_config(string $app_root_dir, array $configs): \Tlf\ErrorHandler`   
- `public function __construct(string $app_root_dir)`   
- `public function log_message(string $user_message, string $debug_message = '', string $log_file  'main.txt', array $args_to_log  [])`   
- `public function determine_print_mode(): \Tlf\ErrorHandler\PrintMode`   
- `public function register_exception_handler()`   
- `public function register_error_log_handler()`   
  
