<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Cli.php  
  
# class Tlf\ErrorHandler\Cli  
  
See source code at [/src/Cli.php](/src/Cli.php)  
  
## Constants  
  
## Properties  
- `protected array $commands = [  
        'init' => "Create a config file and php initialization script",   
  
    ];` array<string command, string help_message>  
  
## Methods   
- `public function init_commands()`   
- `public function run_init()`   
  
